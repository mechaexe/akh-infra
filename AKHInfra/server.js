﻿var allowCrossDomain = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Origin');

	next();
}

var express = require('express'),
	app = express(),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	dbString = 'mongodb://127.0.0.1/PainDB',
	Task = require('./api/models/defaultModel'),
	defaultPort = 8080;

var server = {
	startServer: function (startPort) {

		if (startPort == undefined) {
			startPort = defaultPort;
			console.log("Server port is undefined, using default port: " + startPort);		
		}

		//var port = process.env.PORT || startPort;

		mongoose.Promise = global.Promise;
		mongoose.connect(dbString);

		app.use(allowCrossDomain);
		app.use(bodyParser.urlencoded({ extended: true }));
		app.use(bodyParser.json());

		var routes = require('./api/routes.js');
		routes(app);

		app.listen(startPort, "0.0.0.0");

		console.log("Server listening on port: " + startPort);
		
	},
	stopServer: function () {
		app.removeAllListeners();
		console.log("Server stopped");
	}
};

module.exports = server;