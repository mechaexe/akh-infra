﻿'use strict';

module.exports = function (app) {
	var user = require('./controllers/userController');
    var admin = require('./controllers/adminController');

    // app.route('/user')
    //     .put(admin.updateUser);
        //.get(admin.getUsers);

    // app.route('/user/:code')
	// 	.delete(admin.deleteQuestionnaire);

	app.route('/questionnaires')
		.post(user.postAnswer)
		.put(admin.updateQuestionnaire);
		//.get(admin.getAllQuestionnaires);
	
	app.route('/templates')
		.get(user.getTemplates)
		.put(admin.updateTemplate)
		.post(admin.putTemplate);

	app.route('/templates/:objectId')
		.put(admin.updateTemplate);

	/*app.route('templates/:params')
		.post(admin.postQuestionnaire);
*/
	app.route('/activateTemplate')
		.put(admin.activateTemplate);

	app.route('/answers/:code')
		.get(admin.getQuestionnaire);
};  