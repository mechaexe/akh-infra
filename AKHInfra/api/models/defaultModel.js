﻿﻿'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var freetext = new Schema({
	type: Number,
	required: Boolean,
	question: String,
	description: String,
	answer: String
});

var multipleAnswer = new Schema({
	type: Number,
	answer: String,
	selected: Boolean,
	image: String
});

var multipleAnswerQuestion = new Schema({
	type: Number,
	required: Boolean,
	question: String,
	description: String,
	answers: [multipleAnswer]
});

var questionnaire = new Schema({
	active: Boolean,
	name: String,
	creationDate: Date,
	questions: [Schema.Types.Mixed]
});

var answer = new Schema({
	userCode: String,
	saveDate: Date,
	name: String,
	questions: [Schema.Types.Mixed]
});

//model(<Modelname>, <Schema>, <DBCollection>)
module.exports = mongoose.model('template', questionnaire, 'template');
module.exports = mongoose.model('answers', answer, 'answers');
