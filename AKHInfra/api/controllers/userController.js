﻿'use strict';

var mongoose = require('mongoose'),
	answers = mongoose.model('answers'),
	templates = mongoose.model('template');

exports.getTemplates = function (req, res) {
	if (req.query.type == 'active') {
		templates.find({ "active": "true" }, function (err, quest) {
			if (err) {
				res.status(res).send();
				return;
			}
			res.json(quest);
		});
	}
	else {
		templates.find(function (err, quest) {
			if (err) {
				res.status(res).send();
				return;
			}
			res.json(quest);
		});
	}
};

exports.postAnswer = function (req, res) {
    var data = new answers(req.body);
	data.save()
		.then(item => res.status(200).send())
		.catch(err => res.status(400).send());
};