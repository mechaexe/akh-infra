'use strict';

var mongoose = require('mongoose'),
    Template = mongoose.model('template'),
    Answers = mongoose.model('answers');

/*exports.updateUser = function (request, response) {
    Users.findByIdAndUpdate(request.body['_id'], request.body, function (err, result) {
        if (err) {
            console.log(err);
            return
        }
        response.send("Change Successful");
    })
};*/

exports.deleteQuestionnaire = function (req, res) {
    Template.remove({
        _id: req.body['_id']
    }, function (err, questionnaire) {
        if (err) {
            res.send();
            return;
        }
        res.send("Deleted successfully");
    })
};

exports.updateQuestionnaire = function (req, response) {
    var newQuestionnaire = new Answers(req.body);
    Answers.findByIdAndUpdate(req.body['_id'], {
        $set: newQuestionnaire
    }, function (err, res) {
        if (err) {
            response.send(err);
            return;
        }
        response.json("updated");
    })
};

exports.updateTemplate = function (req, response) {
    var newTemplate = new Template(req.body);
    var objectId;
    if (req.body['_id']) {
        objectId = req.body['_id'];
    } else {
        objectId = req.params.objectId;
    }
    newTemplate._id = objectId;
    Template.findByIdAndUpdate(
        objectId, {
            $set: newTemplate
        },
        function (err, res) {
            if (err) {
                response.send(err);
                return;
            }
            response.json("updated");
        })
};

exports.putTemplate = function (request, response) {
    var newTemplate = new Template(request.body);
    newTemplate.save(function (err, template) {
        if (err) {
            response.send(err);
            return;
        }
        response.json("Inserted successfully");
    })
};

exports.activateTemplate = function (req, response) {
    if (req.body['_id']) {
        Template.update({
            active: true
        }, {
            $set: {
                active: false
            }
        }, function (err, res) {
            if (err) {
                console.log(err);
                return;
            } else {
                Template.findOneAndUpdate({
                    _id: req.body['_id']
                }, {
                    $set: {
                        active: true
                    }
                }, function (err, res) {
                    if (err) {
                        console.log(err);
                        response.send("failed", err);
                    }
                    response.send("Activated successfully");
                })
            }
        })
    }
};

exports.getQuestionnaire = function (req, res) {
    var code = req.params.code;
    if (isNaN(code)) {
        res.json('"error":"code is not a number"');
    } else if (code) {
        Answers.findOne({
            userCode: req.params.code
        }, function (err, answer) {
            if (err) {
                console.log(err);
                res.send("failed", err);
            }
            res.send(answer);
        })
    } else {
        res.send("no code sent");
    }
};

//not activated
exports.getAllQuestionnaires = function (req, response) {
    Answers.find({}, function (err, res) {
        if (err) {
            response.send(err);
        } else {
            response.send(res);
        }
    })
}